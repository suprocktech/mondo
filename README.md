# Mondo

## About
Mondo (formerly ShaftMON) can be used to analyze and graph data collected from Asphodel devices.

The Asphodel communication protocol was developed by Suprock Technologies (http://www.suprocktech.com)

## License
Mondo is licensed under the BSD license.

The BSD license permits usage in both open source and propretary projects.
